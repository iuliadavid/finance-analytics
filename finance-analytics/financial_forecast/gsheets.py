import gspread
import gspread_dataframe as gd
import pandas as pd
from pandas import DataFrame

gs = gspread.oauth()


def create_dataframe_from_url(url: str, sheet_name: str) -> DataFrame:
    sh = gs.open_by_url(url)
    sheet = sh.worksheet(sheet_name)

    values = sheet.get_all_values()
    headers = values.pop(0)

    return pd.DataFrame(values, columns=headers)


def update_gsheet_from_dataframe(df: DataFrame, url: str, sheet_name: str):
    sh = gs.open_by_url(url)
    sheet = sh.worksheet(sheet_name)
    sheet.clear()

    gd.set_with_dataframe(sheet, df)
